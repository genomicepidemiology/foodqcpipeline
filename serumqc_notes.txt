# Instructions to run the serumqc pipeline

# Purge all modules to make sure they are not causing any problems
module purge

# Load necessary modules
module load tools ngs torque moab jre/1.8.0-openjdk perl/5.20.1 bbmap/36.49 ncbi-blast/2.6.0+ mlst/2.9 bowtie2/2.2.5 trimmomatic/0.36 cd-hit/4.6.1 pilon/1.22 bwa/0.7.15 MUMmer/3.23 anaconda3/4.0.0 kraken/0.10.6-unreleased-20160118 samtools/1.2 SPAdes/3.10.1 elprep/2.6 anaconda2/4.0.0 quast/4.5 serumqc/master

# Execute pipeline
# Note: -se -e <email> can be left out. It just sends an email alerting that QC has begun on the run.
serumqc.py -i Raw_data/ -o serumqc/ -run MiSeq_230 -g cge -se -e rkmo@food.dtu.dk



started 11:18



/home/projects/cge/people/csapou/Handling_experiment/20171010_Flowcells_combined/Rolf