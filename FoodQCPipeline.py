#!/usr/bin/env python3

# TODO
# * Add CheckM for contamination control
# * BUG: The adapter.txt file is hard-coded and does not change even if
#   provided the adapters option.

# UPDATES
#
# Version 1.6 released 06-04-2022 :
#   * Added option to set group name "--queue_group".
#   * Updated MoabTorqueSubmission module.
#
# Version 1.5 released 24-01-2020 :
#   * All previous options which required a boolean as argument, does no longer
#     take an argument.
#   * Assume sequences are not nextseq and will per default not concat them.
#     Needs to be specified using the flag "--nextseq" or "-n"
#   * In order to get insert size, one now need to use the flag "insert_size"
#   * Added ability to make script wait for jobs before quiting.
#   * Added ability to adjust walltime for trimmming jobs, also increased it.
#   * Added ability to adjust trim memory.
#   * Added option to adjust minimum read size accepted.
#   * Added option to prevent FastQC from running.
# Version 1.4 released 22-11-2017 :
#   * The pipeline now concats all fastq files from different lanes with
#     identical IDs. For non-NextSeq fastqs these will just be copied.
#   * The pipeline employs bbmap to map all trimmed reads to the MGMapper
#     bacteria database in order to determine insert size (runtime +5 min).
#   * Pipeline now removes several temporary assembly files.
#   * Pipeline will now output version number in first line of output.
#   * Updated bbtools to version 36.49
#   * Updated FastQC to version 0.11.5
#   * Updated SPAdes to version 3.11.0
# Version 1.3 released 07-09-2017 :
#   * Added the posibility to use single end data
#   * Added fastqc calculation after trimming
#   * Releases code to BitBucket
# Version 1.2 released 16-09-2016 :
#   * Added option for more memory in trimming step.
#   * Updated bbmap version.
#   * Added --grade option. This is mostly for FVST.
# Version 1.1 released 16-04-2016 14:51 :
#   * Added "-k 21,33,55,77,99,127" to spades command.
# Version 1.0:
#   * Release

from __future__ import print_function
import argparse
import os.path
import gzip
import sys
import subprocess

from python_module_seqfilehandler.SeqFileHandler import SeqFile
from python_module_dependencies.Dependencies import Dependencies
from python_module_moabtorquesubmission.MoabTorqueSubmission import JobCluster


def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


version_no = "1.6"

#
# Handling arguments
#
parser = argparse.ArgumentParser(description="Runs triming, fastqc, and\
                                 assembly. Used for QC.")
# Posotional arguments
parser.add_argument("input_files",
                    help="Raw data in FASTQ format and/or assemblies in FASTA\
                          format.",
                    nargs='+',
                    metavar='FAST(Q|A)')

parser.add_argument("--adapters",
                    help="Text file containing on adapter on each line.",
                    metavar='ADAP_TXT',
                    default=None)
parser.add_argument("--map_db",
                    help="The pipeline maps all reads to this database in\
                          order to determine insert size and spot\
                          contaminations in WGS samples. WGS samples are\
                          defined as samples that are also assembled using\
                          SPAdes.\n \
                          IMPORTANT: The database must have been indexed with\
                          the bbmap version corresponding to the pipeline and\
                          it must have been indexed using the usemodulo=t\
                          option.",
                    metavar='DB_TXT',
                    default=None)
parser.add_argument("--queue",
                    help="Use a queing system for a computer cluster.",
                    metavar='QUEUE',
                    choices=["Computerome", "None"],
                    default="Computerome")
parser.add_argument("-qg", "--queue_group",
                    help="Specify group name for queue.",
                    metavar='GROUP_NAME',
                    default="cge")
parser.add_argument("--spades",
                    help="If set, the data is assembled with spades.",
                    action="store_true",
                    default=False)
parser.add_argument("--trim_min_length",
                    help="Minimum length of reads to keep.",
                    metavar="INT",
                    type=int,
                    default=50)
parser.add_argument("--trim_output",
                    help="The directory where the trimmed files will be\
                          stored.",
                    metavar='DIR',
                    default="Trimmed")
parser.add_argument("--trim_mem",
                    help="Manually set memory requirements for the trimming\
                          step in gigabytes. This is what will be asked for in\
                          the queue. The actual memory requirement will be\
                          about 85%% of this value. Default is 8 (gb).",
                    metavar='MEM',
                    type=int,
                    default=8)
parser.add_argument("--trim_time",
                    help="Manually set maximum walltime for the trimming\
                          jobs in seconds. Default is 28800s = 8h.",
                    metavar='SECS',
                    type=int,
                    default=28800)
parser.add_argument("--assembly_mem",
                    help="Manually set memory requirements for the assembly\
                          step in gigabytes.",
                    metavar='MEM',
                    type=int,
                    default=16)
parser.add_argument("--assembly_output",
                    help="The directory where the assemblies will be stored",
                    metavar='DIR',
                    default="Assemblies")
parser.add_argument("--qc_output",
                    help="The directory where the qc reports are stored.",
                    metavar='DIR',
                    default="QC")
parser.add_argument("-n", "--nextseq",
                    help="Use this flag if your files are NextSeq fastqs, and\
                          therefore a single sample is spread across 8 files.",
                    action="store_true",
                    default=False)
parser.add_argument("--insert_size",
                    help="Use flag to get insert size for sample. Note that\
                          this process requires a significant amount of\
                          resources and time.",
                    action="store_true",
                    default=False)
parser.add_argument("--tmp_dir",
                    help="The directory where temporary files are stored.",
                    metavar='DIR',
                    default="FoodQCPipeline_tmp")
parser.add_argument("--keep",
                    help="Keep the concatenated fastq files in the tmp dir.",
                    action="store_true",
                    default=False)
parser.add_argument("-c", "--config",
                    help="Path to configuration file for external software.\
                          Default path is:\
                          'FoodQCPipeline_config.txt' in the script\
                          directory. The configuration file\
                          expects a prg name and a path on each line of\
                          the file. Ex.: blat<tab>/usr/bin/blat. If a prg\
                          is found in your env. path. you can simply\
                          specify: prg<tab>prg_alias. Ex.: blat<tab>blat.",
                    metavar="CONF_PATH")
parser.add_argument("--abs_path_adapt",
                    help="On some systems (e.g. queues using nodes across\
                          several clusters) the absolute path created by\
                          python will not reconisable by the node running the\
                          software. The argument for this flag will be removed\
                          from the beginning of absolute paths.\
                          Example: --abs_path_adapt /net/node1\
                                   creates from original path:\
                                        /net/node1/home/something\
                                   a new absolute path:\
                                        /home/something",
                    metavar='PATH',
                    default=None)
parser.add_argument("--wait",
                    help="If set, will not release the process before all jobs\
                          in the queue has finished. It must be set with a\
                          maximum number of seconds to wait. The set number\
                          must be > 0 and 7200 will always be added to the\
                          number given.",
                    metavar='SECS',
                    type=int,
                    default=0)
parser.add_argument("-v", "--version",
                    help="Outputs current version number.",
                    action="store_true",
                    default=False)
parser.add_argument("--grade",
                    help="",
                    action="store_true",
                    default=False)
parser.add_argument("--no_fastqc",
                    help="If enabled, will skip FastQC.",
                    action="store_true",
                    default=False)
parser.add_argument("--clean_tmp",
                    help="If enabled, will delete the temporary files.",
                    action="store_true",
                    default=False)

args = parser.parse_args()

# Print version number and exit
print("FoodQCPipeline version: " + version_no)
if(args.version):
    quit(0)

# Check existence of input files.
for i, input_file in enumerate(args.input_files):
    # Create absolute paths and store them in list
    args.input_files[i] = os.path.abspath(input_file)

    if(not os.path.isfile(input_file)):
        print("Input file not found:", input_file)
        print("NOTE: The programme creates absolute paths from relative\
               paths. If you specified a relative path and got this error\
               then try to specify an absolute path and try again.")
        quit(1)

# Check configuration files.
if(not args.config):
    args.config = os.path.dirname(
        os.path.realpath(__file__)) + "/config/FoodQCPipeline_config.txt"
if(not os.path.isfile(args.config)):
    print("configuration file not found:", args.config)
    quit(1)

# Check adapters file.
if(not args.adapters):
    args.adapters = os.path.dirname(
        os.path.realpath(__file__)) + "/db/adapters.fa"
if(not os.path.isfile(args.adapters)):
    print("Adapters file not found:", args.adapters)
    quit(1)
fastqc_adapter_file = os.path.dirname(
    os.path.realpath(__file__)) + "/db/adapters.txt"

# Check db file.
if(not args.map_db):
    args.map_db = os.path.dirname(
        os.path.realpath(__file__)) + "/db/bacteria"
# if(not os.path.isfile(args.map_db)):
#    print("Map database file not found:", args.map_db)
#    quit(1)
(map_db_dir, map_db_file) = os.path.split(args.map_db)

# Check output_parser script.
output_parser = os.path.dirname(
    os.path.realpath(__file__)) + "/scripts/output_parser.py"

# Check contig_filter script
filter_contigs = os.path.dirname(
    os.path.realpath(__file__)) + "/scripts/filter_contigs.py"

# Check tmp and output directories
args.tmp_dir = os.path.abspath(args.tmp_dir)
args.trim_output = os.path.abspath(args.trim_output)
args.qc_output = os.path.abspath(args.qc_output)
if(args.spades):
    args.assembly_output = os.path.abspath(args.assembly_output)

# Create output and tmp directories
os.makedirs(args.trim_output, exist_ok=True)
os.makedirs(args.qc_output, exist_ok=True)
os.makedirs(args.tmp_dir, exist_ok=True)
concat_file_dir = args.tmp_dir + "/" + "conc_fqs"
os.makedirs(concat_file_dir, exist_ok=True)
if(args.spades):
    os.makedirs(args.assembly_output, exist_ok=True)

# Load dependencies
prgs = Dependencies(args.config, ("bbduk2", "bbmap", "python3", "spades",
                                  "fastqc", "quast", "python"),
                    executables=False)

# Group samples split into different files due to having been sequenced on
# different lanes.
if(args.nextseq):
    file_groups = SeqFile.group_fastqs(args.input_files)
    preprocessed_files = []
else:
    file_groups = {}
    preprocessed_files = args.input_files

# Collect all file groups into pairs. If the data is paired, then it should
# consist of two file groups.
data_pairs = {}
for (name, pair_id), lane_nos in file_groups.items():
    pair_ids = data_pairs.get(name, [])
    pair_ids.append(pair_id)
    data_pairs[name] = pair_ids

# Create commands for concatenating the files from different lanes.
concat_cmds = {}

for (name, pair_id), lane_nos in file_groups.items():
    group = lane_nos.values()
    cmd = (prgs["python3"] + " " + os.path.dirname(os.path.realpath(__file__))
           + "/python_module_seqfilehandler/SeqFileHandler.py"
           + " --concat " + " ".join(group)
           + " --concat_outdir " + concat_file_dir)
    seq_filename = SeqFile.get_read_filename(name + pair_id)
    out_filename = seq_filename + ".fq.gz"
    out_file_path = concat_file_dir + "/" + out_filename

    # Check if a file (pair) already has been processed by the QC pipeline
    is_processed = False
    pair_ids = data_pairs[name]
    for pid in pair_ids:
        filename = SeqFile.get_read_filename(name + pid)
        if(os.path.isfile(args.qc_output + "/" + filename + ".qc.txt")):
            is_processed = True

    if(is_processed):
        print("Found QC report for: " + seq_filename)
        continue

    # File (pair) has not been processed and should be added to the pipeline
    # command.
    preprocessed_files.append(out_file_path)

    # Check if files in the file group has already been concatenated.
    if(not os.path.isfile(out_file_path)):
        concat_cmds[out_filename] = cmd
    else:
        eprint("# Found concatenated file: " + out_file_path)

pipeline_cmd = (prgs["python3"] + " "
                + os.path.dirname(os.path.realpath(__file__))
                + "/analysis_pipeline.py"
                + " --adapters " + args.adapters
                + " --map_db " + args.map_db
                + " --queue " + args.queue
                + " --queue_group " + args.queue_group
                + " --trim_output " + args.trim_output
                + " --assembly_output " + args.assembly_output
                + " --qc_output " + args.qc_output
                + " --tmp_dir " + args.tmp_dir
                + " --config " + args.config
                + " --trim_min_length " + str(args.trim_min_length)
                + " --trim_mem " + str(args.trim_mem)
                + " --trim_time " + str(args.trim_time))
if(args.assembly_mem):
    pipeline_cmd += " --assembly_mem " + str(args.assembly_mem)
if(args.spades):
    pipeline_cmd += " --spades " + str(args.spades)
if(args.grade):
    pipeline_cmd += " --grade "
if(args.no_fastqc):
    pipeline_cmd += " --no_fastqc "
if(args.clean_tmp):
    pipeline_cmd += " --clean_tmp "
if(args.insert_size):
    pipeline_cmd += " --insert_size "
if(args.wait):
    pipeline_cmd += " --wait " + str(args.wait) + " "
if(not args.keep and args.nextseq):
    pipeline_cmd += " --delete"

pipeline_cmd += " " + " ".join(preprocessed_files)

if(args.queue == "Computerome"):

    jobs = JobCluster(W="group_list={}".format(args.queue_group),
                      A=args.queue_group, V=False, d=args.tmp_dir,
                      outdir=args.tmp_dir, errdir=args.tmp_dir,
                      scripts=args.tmp_dir, job_interval=1,
                      use_full_job_id=True)

    pipeline_dependencies = []

    for filename, concat_cmd in concat_cmds.items():
        concat_job_id = filename + "_concat"
        jobs.addJob(jobid=concat_job_id,
                    cmd=concat_cmd, nodes=1, ppn=2,
                    mem="3900m", walltime=14400)
        pipeline_dependencies.append(concat_job_id)

    if(not args.nextseq):
        pipeline_dependencies = ""

    jobs.addJob(jobid="foodqcpipeline",
                cmd=pipeline_cmd, nodes=1, ppn=1,
                mem="900m", walltime=200000 + args.wait,
                depend=pipeline_dependencies)

    if(args.wait):
        jobs.run(job_wait=True, max_wait=args.wait)
    else:
        jobs.run()
else:
    for filename, concat_cmd in concat_cmds.items():
        subprocess.run(concat_cmd, shell=True)

    subprocess.run(pipeline_cmd)

quit(0)
