# FoodQCPipeline #

## Introduction ##

Data sequenced at the genepi group at DTU Food will automatically go through this pipeline, which trims low quality data and adaptor sequence. For WGS data the pipeline also does a de novo assembly using SPAdes.

The pipeline is written to be executed on Computerome at DTU. It could be modified to run elsewhere but this would require some work.

**Maintainer:** Rolf Sommer Kaas

## Pipeline overview ##

1. Run FastQC
2. Trim data with bbduk2
3. Run FastQC on trimmed data
4. Map reads to bacteria database (To find insert size. Only first 2 mio reads.)
5. If WGS data run SPAdes

## Pipeline details ##

### Trimming ###

Trimming is done by bbduk2 part of the suite bbtools version 36.49 (https://jgi.doe.gov/data-and-tools/bbtools/).

bbduk2 uses a 19mer to look for contaminating adapters, with the exception of the read tips where kmers down to 11 are used. The adaptors searched for are found in the adapters database inside the "db" folder of the pipeline.

The right end of each read is trimmed to remove bases with a phred score below 20.

Reads shorter than 50 bp. will be discarded (can be found in the discarded file in the "Trimmed" directory).

BBDuk2 will also trim adapters based on where paired reads overlap.

### FastQC ###

FastQC is version 0.11.5 (http://www.bioinformatics.babraham.ac.uk/projects/fastqc/).

FastQC is run twice. Once before trimming and once after. The reports are stored in the "QC" folder.

### Output ###

**Trimmed directory**

For each sample there exists 2 trimmed fastq files (1 if single-end), along with a file containing discarded reads, and if paired-end a file containing reads that could not be paired.

**Assembly directory**

An assembly in FASTA format for each WGS sample. The assembly only contains contigs greater thean 500 bp. For each assembly exists a directory with the details of the assembly.

**QC directory**

For each sample there exists a single line text file, with all the QC information. The headers are as follows:

|     Header                     | Explanation                                                                                       |
| -------------------------------|---------------------------------------------------------------------------------------------------|
| Sample                         | Name of sample, extracted from filename                                                           |
| Bases (MB)                     | Total number of mega bases assigned/called                                                        |
| Qual Bases (MB)                | Total number of mega bases left after trimming                                                    |
| Qual bases %                   | Percentage of bases left after trimming                                                           |
| Reads                          | Total number of reads                                                                             |
| Qual reads                     | Total number of reads left after trimming                                                         |
| Qual reads %                   | Percentage of reads left after trimming                                                           |
| Most common adapter (count)    | Number of times the most comonly encountered adapter was found/trimmed                            |
| 2. Most common adapter (count) | Number of times the second most comonly encountered adapter was found/trimmed                     |
| Other adapters (count)         | Number of times other adapters was found/trimmed                                                  |
| insert size                    | [Insert size](http://thegenomefactory.blogspot.dk/2013/08/paired-end-read-confusion-library.html) |
| N50                            | [N50](https://en.wikipedia.org/wiki/N50%2C_L50%2C_and_related_statistics)                         |
| No ctgs                        | Number of contigs above 500 bp.                                                                   |
| longest                        | Size (in bp.) of the longest contig                                                               |
| total bps                      | Number of bp. found in the assembly with contigs greater than 500 bp.                             |

*Tip*: To create a table of several samples use the cat command and the header file found inside the pipeline folder:
```
cat /home/projects/cge/apps/foodqcpipeline/output_headers.txt QC/*.qc.txt > qc_summary.txt
```

For each sample, two directories exists with the results of FastQC; \*\_fastqc\_pre and \*\_fastqc\_post, contains the FastQC results from before trimming and after trimming, respectively.

If an assembly was done, a directory ending in \_asm\_report exists with detailed assembly metrics.

**FoodQCPipeline\_tmp**

The exact commands used in a specific run can be found in the \*.sh files named according the sample names inferred.

The mapping results done by BBMap for each sample can be found in \*_bbmap_db_stat.txt files.

## Run default pipeline ##

Make sure you are using Python 3 and not 2.7.

```
python FoodQCPipeline.py --spades True Raw_data/*.gz
```

Leave out the spades option if you don't have WGS data.

## Advanced options ##

Use the -h flag to see all the possible options.

```
python FoodQCPipeline.py -h
usage: FoodQCPipeline.py [-h] [--adapters ADAP_TXT] [--map_db DB_TXT]
                         [--queue QUEUE] [--spades BOOL] [--trim_output DIR]
                         [--trim_mem MEM] [--assembly_output DIR]
                         [--qc_output DIR] [--tmp_dir DIR] [--keep]
                         [-c CONF_PATH] [--abs_path_adapt PATH]
                         [--grade GRADE] FAST(Q|A) [FAST(Q|A) ...]

Runs triming, fastqc, and assembly. Used for QC.

positional arguments:
  FAST(Q|A)             Raw data in FASTQ format and/or assemblies in FASTA
                        format.

optional arguments:
  -h, --help            show this help message and exit
  --adapters ADAP_TXT   Text file containing on adapter on each line.
  --map_db DB_TXT       The pipeline maps all reads to this database in order
                        to determine insert size and spot contaminations in
                        WGS samples. WGS samples are defined as samples that
                        are also assembled using SPAdes. IMPORTANT: The
                        database must have been indexed with the bbmap version
                        corresponding to the pipeline and it must have been
                        indexed using the usemodulo=t option.
  --queue QUEUE         Use a queing system for a computer cluster.
  --spades BOOL         If set the data assembled with spades.
  --trim_output DIR     The directory where the trimmed files will be stored.
  --trim_mem MEM        Manually set memory requirements for the trimming step
                        in gigabytes.
  --assembly_output DIR
                        The directory where the assemblies will be stored
  --qc_output DIR       The directory where the qc reports are stored.
  --tmp_dir DIR         The directory where temporary files are stored.
  --keep                Keep the concatenated fastq files in the tmp dir.
```

## Suggestions and Improvements ##

Do not hesitate to let me know if you have ideas for improvements or extra functionality that could be useful.
