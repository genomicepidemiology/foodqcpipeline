#! /tools/bin/python3

import argparse
import os.path
import os
import sys
import re
import math


NO_ASSEMBLY = "no assembly"


def calc_grade(out_contig_count, out_qbases, out_n50):

    grades_dict = {
        1: "C",
        2: "B",
        3: "A",
        4: "A+",
        5: "A++",
        6: "A+++"}

    out_str = grades_dict[1]

    if(out_contig_count == NO_ASSEMBLY):
        return out_str

    out_contig_count = int(out_contig_count)
    out_qbases = int(out_qbases)
    out_n50 = int(out_n50)

    if(out_contig_count >= 1000):
        return out_str
    if(out_qbases < 100):
        return out_str
    if(out_n50 < 15000):
        return out_str

    out_str = grades_dict[2]
    if(out_qbases < 150):
        return out_str
    if(out_n50 < 30000):
        return out_str

    out_str = grades_dict[3]
    if(out_qbases < 250):
        return out_str
    if(out_n50 < 100000):
        return out_str

    out_str = grades_dict[4]
    if(out_qbases < 375):
        return out_str

    out_str = grades_dict[5]
    if(out_qbases < 500):
        return out_str

    return grades_dict[6]


#
# Handling arguments
#
parser = argparse.ArgumentParser(description="Parses outputs from the\
                                 applications run with the FoodQCPipeline.")

parser.add_argument("--output_file",
                    help="Tab seperated file containing the parsed results.",
                    metavar='FILE')
parser.add_argument("--output_fail",
                    help="Create a file with this name if prg fails.",
                    default="failed.txt",
                    metavar='FILE')
parser.add_argument("--trimk_input",
                    help="The directory where the trimmed files will be\
                          stored",
                    metavar='TXT')
parser.add_argument("--trimq_input",
                    help="The directory where the trimmed files will be\
                          stored",
                    metavar='TXT')
parser.add_argument("--assembly_input",
                    help="The directory where the assemblies will be stored",
                    metavar='TXT',
                    default=None)
parser.add_argument("--insert_file",
                    help="",
                    metavar='TXT',
                    default=None)
parser.add_argument("--sample_name",
                    help="",
                    metavar='TXT',
                    default=None)
parser.add_argument("--grade",
                    help="",
                    type=bool,
                    default=False)

args = parser.parse_args()

if(not os.path.exists(args.trimk_input)):
    with open(args.output_fail, "w") as fail_fh:
        fail_fh.write("")
    sys.exit("Output parser did not find a trimk file, which indicated "
             "that the trimming has failed.")
# Job failed previously but has now suceeded
elif(os.path.exists(args.output_fail)):
    os.remove(args.output_fail)

out_bases = ""
out_qbases = ""
out_qbases_pct = ""
out_reads = ""
out_qreads = ""
out_qreads_pct = ""

# Capture QC data from qual trimming.
with open(args.trimq_input, "r", encoding="utf-8") as trim_fh:

    re_input = re.compile(r"Input:\s+(\d+) reads\s+(\d+) bases.")
    re_trim = re.compile(r"Result:\s+(\d+) reads \((.+?%)\)\s+(\d+) bases "
                         + r"\((.+?%)\)")

    for line in trim_fh:
        match_input = re_input.search(line)

        if(match_input):
            out_reads = match_input.group(1)
            out_bases = match_input.group(2)
            out_bases = (int(out_bases) / 1000000)
            out_bases = math.floor(out_bases)
            continue

        match_trim = re_trim.search(line)
        if(match_trim):
            out_qreads = match_trim.group(1)
            out_qreads_pct = match_trim.group(2)
            out_qbases = match_trim.group(3)
            out_qbases = (int(out_qbases) / 1000000)
            out_qbases = math.floor(out_qbases)
            out_qbases_pct = match_trim.group(4)
            continue

out_sample_name = ""
out_adapter1_count = ""
out_adapter2_count = ""
out_adapter_other_count = ""

# Capture QC data from adapter trimming.
with open(args.trimk_input, "r", encoding="utf-8") as trim_fh:

    re_input = re.compile(r"Input:\s+(\d+) reads\s+(\d+) bases.")

    # Get sample name
    file_list = trim_fh.readline().split("\t")
    file_path = file_list[1]
    filename = os.path.basename(file_path).rstrip()
    if(args.sample_name):
        out_sample_name = args.sample_name
    else:
        out_sample_name = filename

    # Skip line.
    trim_fh.readline()

    # Total matched.
    total_list = trim_fh.readline().split("\t")
    total_matched = total_list[1]

    # Skip line.
    trim_fh.readline()

    # First adapter.
    adapter1_list = trim_fh.readline().split("\t")

    if(len(adapter1_list) > 1):
        out_adapter1_count = adapter1_list[1]
    else:
        out_adapter1_count = 0

    # Second adapter.
    adapter2_list = trim_fh.readline().split("\t")

    if(len(adapter2_list) > 1):
        out_adapter2_count = adapter2_list[1]
    else:
        out_adapter2_count = 0

    out_adapter_other_count = (int(total_matched) - int(out_adapter1_count)
                               - int(out_adapter2_count))

out_insert_size_median = ""
out_insert_size_avg = ""

# Only for paired-end data.
if(args.insert_file is not None and os.path.isfile(args.insert_file)):
    with open(args.insert_file, "r", encoding="utf-8") as ins_fh:
        for line in ins_fh:
            if(line.startswith("insert size avg:")):
                line_array = line.split("\t")
                out_insert_size_avg = line_array[1].strip()

out_contig_count = ""
out_n50 = ""
out_longest_contig = ""
out_asm_length = ""

# Only for WGS.
if(args.assembly_input and os.path.getsize(args.assembly_input) == 0):
    out_contig_count = NO_ASSEMBLY
    out_longest_contig = NO_ASSEMBLY
    out_asm_length = NO_ASSEMBLY
    out_n50 = NO_ASSEMBLY
elif(args.assembly_input):
    with open(args.assembly_input, "r", encoding="utf-8") as asm_fh:

        re_number = re.compile(r"\s+(\d+)\s*$")

        # Skip a lot of details in the beginning
        asm_fh.readline()
        asm_fh.readline()
        asm_fh.readline()
        asm_fh.readline()
        asm_fh.readline()
        asm_fh.readline()
        asm_fh.readline()
        asm_fh.readline()
        asm_fh.readline()
        asm_fh.readline()
        asm_fh.readline()
        asm_fh.readline()
        asm_fh.readline()
        asm_fh.readline()
        asm_fh.readline()

        # Number of contigs.
        contig_count_line = asm_fh.readline()
        match_contigs = re_number.search(contig_count_line)
        out_contig_count = match_contigs.group(1)

        # Longest contig
        longest_contig_line = asm_fh.readline()
        match_longest_contig = re_number.search(longest_contig_line)
        out_longest_contig = match_longest_contig.group(1)

        # Assembly_length.
        asm_length_line = asm_fh.readline()
        match_asm_length = re_number.search(asm_length_line)
        out_asm_length = match_asm_length.group(1)

        # Skip GC content.
        asm_fh.readline()

        # N50
        n50_line = asm_fh.readline()
        match_n50 = re_number.search(n50_line)
        out_n50 = match_n50.group(1)

out_grade = ""
if(args.grade and args.assembly_input):
    out_grade = calc_grade(out_contig_count, out_qbases, out_n50)

if(args.grade != ""):
    out_grade = "\t" + out_grade

with open(args.output_file, "w", encoding="utf-8") as out_fh:
    out_fh.write(out_sample_name + "\t" +
                 str(out_bases) + "\t" +
                 str(out_qbases) + "\t" +
                 str(out_qbases_pct) + "\t" +
                 str(out_reads) + "\t" +
                 str(out_qreads) + "\t" +
                 str(out_qreads_pct) + "\t" +
                 str(out_adapter1_count) + "\t" +
                 str(out_adapter2_count) + "\t" +
                 str(out_adapter_other_count) + "\t" +
                 str(out_insert_size_avg) + "\t" +
                 str(out_n50) + "\t" +
                 str(out_contig_count) + "\t" +
                 str(out_longest_contig) + "\t" +
                 str(out_asm_length) +
                 out_grade + "\n")

quit(0)
