#! /tools/bin/python3

import argparse
import os.path
import sys
import re
import subprocess

from SeqFileHandler import SeqFile

#
# Handling arguments
#
parser = argparse.ArgumentParser(description="Parses outputs from the\
                                 applications run with the FoodQCPipeline.")

parser.add_argument("-i", "--input_names",
                    help="Tab seperated file containing:\
                    filename<tab>new_filename. One line per file.",
                    metavar='TXT')
parser.add_argument("-o", "--output_dir",
                    help="Directory to move the renamed files to.",
                    metavar='DIR')
# Posotional arguments
parser.add_argument("input_files",
                    help="Raw data in FASTQ format and/or assemblies in FASTA\
                          format.",
                    nargs='+',
                    metavar='FAST(Q|A)')

args = parser.parse_args()

# Load files and pair them if necessary
files = SeqFile.parse_files(args.input_files)

names_dict = {}

# Capture QC data from qual trimming.
with open(args.input_names, "r", encoding="utf-8") as in_fh:

    for line in in_fh:
        names = line.split("\t")
        names_dict[names[0]] = names[1].strip()

# Because second + is gredy it will grab all extensions.
re_extensions = re.compile(r"(\..+)$")

for seqfile in files:

    for old_name in names_dict:

        if(old_name in seqfile.path):

            filename = os.path.basename(seqfile.path)
            match_extensions = re_extensions.search(filename)
            file_extensions = ""
            if(match_extensions):
                file_extensions = match_extensions.group(1)

            cmd = ("mv " + seqfile.path + " " + args.output_dir + "/" +
                   names_dict[old_name])

            if(seqfile.seq_format == "paired"):
                cmd2 = ("mv " + seqfile.pe_file_reverse + " " +
                        args.output_dir + "/" + names_dict[old_name] +
                        "_R2" + file_extensions)
                cmd += "_R1"

            cmd += file_extensions

            print(cmd, file=sys.stderr)
            print(cmd2, file=sys.stderr)
            subprocess.check_call([cmd], shell=True)
            subprocess.check_call([cmd2], shell=True)

quit(0)
