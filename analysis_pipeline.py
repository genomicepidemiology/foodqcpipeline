#!/usr/bin/env python3

# TODO
# * BUG: The adapter.txt file is hard-coded and does not change even if
#   provided the adapters option.

from __future__ import print_function
import argparse
import os.path
import gzip
import sys
import subprocess

from python_module_seqfilehandler.SeqFileHandler import SeqFile
from python_module_dependencies.Dependencies import Dependencies
from python_module_moabtorquesubmission.MoabTorqueSubmission import JobCluster


def eprint(*args, **kwargs):
    print(*args, file=sys.stderr, **kwargs)


version_no = "1.6"

#
# Handling arguments
#
parser = argparse.ArgumentParser(description="Runs triming, fastqc, and\
                                 assembly. Used for QC.")
# Posotional arguments
parser.add_argument("input_files",
                    help="Raw data in FASTQ format and/or assemblies in FASTA\
                          format.",
                    nargs='+',
                    metavar='FAST(Q|A)')

parser.add_argument("--adapters",
                    help="Text file containing on adapter on each line.",
                    metavar='ADAP_TXT',
                    default=None)
parser.add_argument("--map_db",
                    help="The pipeline maps all reads to this database in\
                          order to determine insert size and spot\
                          contaminations in WGS samples. WGS samples are\
                          defined as samples that are also assembled using\
                          SPAdes.\n \
                          IMPORTANT: The database must have been indexed with\
                          the bbmap version corresponding to the pipeline and\
                          it must have been indexed using the usemodulo=t\
                          option.",
                    metavar='DB_TXT',
                    default=None)
parser.add_argument("--queue",
                    help="Use a queing system for a computer cluster.",
                    metavar='QUEUE',
                    choices=["Computerome", "None"],
                    default="Computerome")
parser.add_argument("-qg", "--queue_group",
                    help="Specify group name for queue.",
                    metavar='GROUP_NAME',
                    default="cge")
parser.add_argument("--spades",
                    help="If set the data assembled with spades.",
                    metavar='BOOL',
                    type=bool,
                    default=False)
parser.add_argument("--trim_min_length",
                    help="Minimum length of reads to keep.",
                    metavar="INT",
                    type=int,
                    default=50)
parser.add_argument("--trim_output",
                    help="The directory where the trimmed files will be\
                          stored.",
                    metavar='DIR',
                    default="Trimmed")
parser.add_argument("--trim_mem",
                    help="Manually set memory requirements for the trimming\
                          step in gigabytes. This is what will be asked for in\
                          the queue. The actual memory requirement will be\
                          about 85%% of this value. Default is 8 (gb).",
                    metavar='MEM',
                    type=int,
                    default=8)
parser.add_argument("--trim_time",
                    help="Manually set maximum walltime for the trimming\
                          jobs in seconds. Default is 28800s = 8h.",
                    metavar='SECS',
                    type=int,
                    default=28800)
parser.add_argument("--assembly_mem",
                    help="Manually set memory requirements for the assembly\
                          step in gigabytes.",
                    metavar='MEM',
                    type=int,
                    default=16)
parser.add_argument("--assembly_output",
                    help="The directory where the assemblies will be stored",
                    metavar='DIR',
                    default="Assemblies")
parser.add_argument("--qc_output",
                    help="The directory where the qc reports are stored.",
                    metavar='DIR',
                    default="QC")
parser.add_argument("--tmp_dir",
                    help="The directory where temporary files are stored.",
                    metavar='DIR',
                    default="FoodQCPipeline_tmp")
parser.add_argument("--insert_size",
                    help="Use flag to get insert size for sample. Note that\
                          this process requires a significant amount of\
                          resources and time.",
                    action="store_true",
                    default=False)
parser.add_argument("--delete",
                    help="Delete input files.",
                    action="store_true",
                    default=False)
parser.add_argument("-c", "--config",
                    help="Path to configuration file for external software.\
                          Default path is:\
                          'FoodQCPipeline_config.txt' in the script\
                          directory. The configuration file\
                          expects a prg name and a path on each line of\
                          the file. Ex.: blat<tab>/usr/bin/blat. If a prg\
                          is found in your env. path. you can simply\
                          specify: prg<tab>prg_alias. Ex.: blat<tab>blat.",
                    metavar="CONF_PATH")
parser.add_argument("--abs_path_adapt",
                    help="On some systems (e.g. queues using nodes across\
                          several clusters) the absolute path created by\
                          python will not reconisable by the node running the\
                          software. The argument for this flag will be removed\
                          from the beginning of absolute paths.\
                          Example: --abs_path_adapt /net/node1\
                                   creates from original path:\
                                        /net/node1/home/something\
                                   a new absolute path:\
                                        /home/something",
                    metavar='PATH',
                    default=None)
parser.add_argument("--wait",
                    help="If set, will not release the process before all jobs\
                          in the queue has finished. It must be set with a\
                          maximum number of seconds to wait.",
                    metavar='SECS',
                    type=int,
                    default=0)
parser.add_argument("-v", "--version",
                    help="Outputs current version number.",
                    action="store_true",
                    default=False)
parser.add_argument("--grade",
                    help="",
                    action="store_true",
                    default=False)
parser.add_argument("--no_fastqc",
                    help="If enabled, will skip FastQC.",
                    action="store_true",
                    default=False)
parser.add_argument("--clean_tmp",
                    help="If enabled, will delete the temporary files.",
                    action="store_true",
                    default=False)

args = parser.parse_args()

# Print version number and exit
if(args.version):
    print("FoodQCPipeline version: " + version_no)
    quit(0)
else:
    print("FoodQCPipeline version: " + version_no)

# Check configuration files.
if(not args.config):
    args.config = os.path.dirname(
        os.path.realpath(__file__)) + "/config/FoodQCPipeline_config.txt"
if(not os.path.isfile(args.config)):
    print("configuration file not found:", args.config)
    quit(1)

# Check adapters file.
if(not args.adapters):
    args.adapters = os.path.dirname(
        os.path.realpath(__file__)) + "/db/adapters.fa"
if(not os.path.isfile(args.adapters)):
    print("Adapters file not found:", args.adapters)
    quit(1)
fastqc_adapter_file = os.path.dirname(
    os.path.realpath(__file__)) + "/db/adapters.txt"

# Check db file.
if(not args.map_db):
    args.map_db = os.path.dirname(
        os.path.realpath(__file__)) + "/db/bacteria"
# if(not os.path.isfile(args.map_db)):
#    print("Map database file not found:", args.map_db)
#    quit(1)
(map_db_dir, map_db_file) = os.path.split(args.map_db)

# Check output_parser script.
output_parser = os.path.dirname(
    os.path.realpath(__file__)) + "/scripts/output_parser.py"

# Check contig_filter script
filter_contigs = os.path.dirname(
    os.path.realpath(__file__)) + "/scripts/filter_contigs.py"

# Check tmp and output directories
args.tmp_dir = os.path.abspath(args.tmp_dir)
args.trim_output = os.path.abspath(args.trim_output)
args.qc_output = os.path.abspath(args.qc_output)
if(args.spades):
    args.assembly_output = os.path.abspath(args.assembly_output)

# Create output and tmp directories
os.makedirs(args.trim_output, exist_ok=True)
os.makedirs(args.qc_output, exist_ok=True)
os.makedirs(args.tmp_dir, exist_ok=True)
if(args.spades):
    os.makedirs(args.assembly_output, exist_ok=True)

# Load dependencies
prgs = Dependencies(args.config, ("bbduk2", "bbmap", "python3", "spades",
                                  "fastqc", "quast", "python"),
                    executables=False)

# Load files and pair them if necessary
files = SeqFile.parse_files(args.input_files)

# Create all cmds needed for the job object.
trim_cmds = {}
fastqc_cmds = {}
parser_cmds = {}
spades_cmds = {}
bbmap_cmds = {}

files_for_submission = []

for seqfile in files:
    if(os.path.isfile(args.qc_output + "/" + seqfile.filename + ".qc.txt")):
        print("Found QC report for "
              + args.qc_output + "/" + seqfile.filename + ".qc.txt")
        continue
    else:
        # Maintain a list of files with related jobs.
        files_for_submission.append(seqfile)

    trim_mem_actual = round(int(args.trim_mem) * 0.85)
    q_trim_mem = str(args.trim_mem) + "gb"

    # Trimming cmd general part.
    trim_cmd = (prgs["bbduk2"] +
                " qin=auto" +
                " k=19" +
                " rref=" + args.adapters +
                " mink=11" +
                " qtrim=r" +
                " trimq=20" +
                " minlength=" + str(args.trim_min_length) +
                " tbo" +
                " ziplevel=6" +
                " overwrite=t" +
                " in=" + seqfile.path +
                " out=" +
                args.trim_output + "/" +
                seqfile.filename + ".trim.fq.gz" +
                " outm=" +
                args.trim_output + "/" +
                seqfile.filename + ".discarded.fq.gz" +
                " statscolumns=5" +
                " stats=" +
                args.trim_output + "/" +
                seqfile.filename + ".trimk_stats.txt" +
                " -Xmx" + str(trim_mem_actual) + "g")

    trim_cmd = ("/usr/bin/time -v -o {}/{}.trim.fq.gz.time {}"
                .format(args.tmp_dir, seqfile.filename, trim_cmd))

    # FastQC cmd general part
    fastqc_pre_dir = args.qc_output + "/" + seqfile.filename + "_fastqc_pre"
    os.makedirs(fastqc_pre_dir, exist_ok=True)

    fastqc_cmd = (prgs["fastqc"] +
                  " --outdir " + fastqc_pre_dir +
                  " --noextract" +
                  " --threads 2" +
                  " --quiet" +
                  " --contaminants " + fastqc_adapter_file +
                  " " + seqfile.path)

    # Output parser general part.
    parser_cmd = (prgs["python3"] + " " + output_parser +
                  " --trimk_input " +
                  args.trim_output + "/" +
                  seqfile.filename + ".trimk_stats.txt"
                  " --trimq_input " +
                  args.trim_output + "/" +
                  seqfile.filename + ".trimq_stats.txt")
    if(args.grade):
        parser_cmd += " --grade True"

    # Command only used with paired-end seqs.
    bbmerge_cmd = ""

    if(seqfile.seq_format == "paired"):

        # Trim cmd paired-end part.
        trim_cmd += (" outs=" +
                     args.trim_output + "/" +
                     seqfile.filename + ".singletons.fq.gz" +
                     " in2=" + seqfile.pe_file_reverse +
                     " out2=" + args.trim_output + "/" +
                     seqfile.filename_reverse + ".trim.fq.gz")

        # FastQC cmd paired-end part.
        fastqc_cmd += " " + seqfile.pe_file_reverse

        # Sets paths to trimmed files that does not exist yet.
        seqfile.set_trim_files(args.trim_output + "/" +
                               seqfile.filename + ".trim.fq.gz",
                               args.trim_output + "/" +
                               seqfile.filename_reverse + ".trim.fq.gz")

        # Output parser paired-end part.
        parser_cmd += (" --insert_file " + args.tmp_dir + "/" +
                       seqfile.filename + "_bbmap.txt")
    else:
        # Sets paths to trimmed files that does not exist yet.
        seqfile.set_trim_files(args.trim_output + "/" +
                               seqfile.filename + ".trim.fq.gz")

    # Trim cmd final part.
    trim_cmd += (" &>" + args.trim_output + "/" +
                 seqfile.filename + ".trimq_stats.txt\n")

    # FastQC cmd final part.
    fastqc_cmd += "\n"

    if(not args.no_fastqc):
        # Adding a fastqc after trimming.

        fastqc_post_dir = "{qc:s}/{filename:s}_fastqc_post".format(
            qc=args.qc_output, filename=seqfile.filename)

        os.makedirs(fastqc_post_dir, exist_ok=True)
        fastqc_cmd_post = (prgs["fastqc"] +
                           " --outdir " + fastqc_post_dir +
                           " --noextract" +
                           " --threads 2" +
                           " --quiet" +
                           " --contaminants " + fastqc_adapter_file +
                           " " + seqfile.trim_path)
        if(seqfile.trim_pe_file_reverse):
            fastqc_cmd_post += " " + seqfile.trim_pe_file_reverse

        trim_cmd += fastqc_cmd_post + "\n"
    else:
        trim_cmd += "\n"

    # BBMap cmd
    bbmap_cmd = (prgs["bbmap"] +
                 " k=13" +
                 " usemodulo=t" +
                 " fast=t" +
                 " maxindel=3" +
                 " minhits=2" +
                 " threads=16" +
                 " rescuedist=800" +
                 " usejni=t" +
                 " semiperfectmode=t" +
                 " unpigz=t" +
                 " ambiguous=all" +
                 " reads=2000000" +
                 " printunmappedcount" +
                 " path=" + map_db_dir +
                 " ref=" + args.map_db +
                 " scafstats=" + args.tmp_dir + "/" +
                 seqfile.filename + "_bbmap_db_stat.txt" +
                 " -Xmx99000m" +
                 " in=" + seqfile.trim_path)

    if(seqfile.seq_format == "paired"):
        bbmap_cmd += " in2=" + seqfile.trim_pe_file_reverse

    # if(not args.spades):
        # bbmap_cmd += " reads=10000000"
        # bbmap_cmd += " samplerate=0.5"

    bbmap_cmd += " &> " + args.tmp_dir + "/" + seqfile.filename + "_bbmap.txt"

    # Create spades assembly command
    if(args.spades):
        spades_cmd = (prgs["spades"] +
                      # Kmers to use. These are adopted for Illumina 150-250 bp
                      " -k 21,33,55,77,99,127" +
                      # Output directory
                      " -o " + args.assembly_output + "/" + seqfile.filename +
                      # Tries to reduce the number of mismatches + short indels
                      " --careful" +
                      # Trashes contigs not covered by at least two kmers.
                      " --cov-cutoff 2.0"
                      # Threads. Default is 16.
                      " -t 4" +
                      # Memory in gb. Default is 250
                      " -m " + str(args.assembly_mem) +
                      # Directory in which the corected reads are stored.
                      " --tmp-dir " + args.assembly_output +
                      "/corrected_reads/" + seqfile.filename)
        if(seqfile.seq_format == "paired"):
            spades_cmd += (" -1 " + seqfile.trim_path +
                           " -2 " + seqfile.trim_pe_file_reverse)
        else:
            spades_cmd += " -s " + seqfile.trim_path

        spades_cmd += "\n"

        # Analyze contigs
        spades_cmd += (prgs["python3"] + " " + prgs["quast"] +
                       " --threads 1" +
                       " -o " + args.qc_output + "/" +
                       seqfile.filename + "_asm_report" +
                       " " + args.assembly_output + "/" +
                       seqfile.filename + "/contigs.fasta\n")

        # Filters contigs < 500 bp
        spades_cmd += (prgs["python3"] + " " + filter_contigs +
                       " 500" +
                       " " + args.assembly_output + "/" +
                       seqfile.filename + "/contigs.fasta\n")

        # Copy contigs to output dir.
        spades_cmd += ("cp " +
                       args.assembly_output + "/" +
                       seqfile.filename + "/contigs.filter500.fasta " +
                       args.assembly_output + "/" + seqfile.filename + ".fa\n")

        # Clean up spades files
        spades_cmd += ("rm -r " + args.assembly_output + "/corrected_reads/" +
                       seqfile.filename + "\n" +
                       "rm " + args.assembly_output + "/" + seqfile.filename +
                       "/assembly_graph.fastg\n" +
                       "rm " + args.assembly_output + "/" + seqfile.filename +
                       "/*.fasta\n" +
                       "rm " + args.assembly_output + "/" + seqfile.filename +
                       "/*.paths\n" +
                       "rm -r " + args.assembly_output + "/" +
                       seqfile.filename + "/corrected\n" +
                       "rm -r " + args.assembly_output + "/" +
                       seqfile.filename + "/K*\n" +
                       "rm -r " + args.assembly_output + "/" +
                       seqfile.filename + "/mis*\n")

        spades_cmds[seqfile.path] = spades_cmd

        # Output parser assembly part.
        parser_cmd += (" --assembly_input " + args.qc_output + "/" +
                       seqfile.filename + "_asm_report/report.txt")

    # Output parser final part.
    parser_cmd += (" --output_file " + args.qc_output + "/"
                   + seqfile.filename + ".qc.txt"
                   + " --output_fail " + args.qc_output + "/"
                   + seqfile.filename + ".fail.txt\n")

    # Delete input files
    if(args.delete):
        parser_cmd += "rm " + seqfile.path + "\n"
        if(seqfile.seq_format == "paired"):
            parser_cmd += "rm " + seqfile.pe_file_reverse + "\n"

    if(args.clean_tmp):
        parser_cmd += "rm " + args.tmp_dir + "/" + seqfile.filename + "*\n"

    # Adds cmds to respective dicts.
    trim_cmds[seqfile.path] = trim_cmd
    fastqc_cmds[seqfile.path] = fastqc_cmd
    bbmap_cmds[seqfile.path] = bbmap_cmd
    parser_cmds[seqfile.path] = parser_cmd

# Only keep files which contains jobs for submission.
files = files_for_submission

job_count = len(list(parser_cmds.keys()))
if(job_count == 0):
    print("No jobs to submit")
    quit(0)
else:
    print("Submitting " + str(job_count) + " jobs (x5)")

if(args.queue == "Computerome"):

    jobs = JobCluster(W="group_list={}".format(args.queue_group),
                      A=args.queue_group, V=False, d=args.tmp_dir,
                      outdir=args.tmp_dir, errdir=args.tmp_dir,
                      scripts=args.tmp_dir, job_interval=1,
                      use_full_job_id=True, ignore_queue_load=True)

    for seqfile in files:
        parser_dependencies = []
        bbmap_spades_dependencies = []

        # Add trim jobs
        trim_job_id = seqfile.filename + "_trim"
        jobs.addJob(jobid=trim_job_id,
                    cmd=trim_cmds[seqfile.path], nodes=1, ppn=2,
                    mem=q_trim_mem, walltime=args.trim_time)
        parser_dependencies.append(trim_job_id)
        bbmap_spades_dependencies.append(trim_job_id)

        # Adding FastQC jobs.
        fastqc_job_id = seqfile.filename + "_fastqc"
        jobs.addJob(jobid=fastqc_job_id,
                    cmd=fastqc_cmds[seqfile.path], nodes=1, ppn=2,
                    mem="1900m", walltime=3600)
        parser_dependencies.append(fastqc_job_id)

        # Adding BBMap jobs.
        if(args.insert_size is True):
            bbmap_job_id = seqfile.filename + "_bbmap"
            jobs.addJob(jobid=bbmap_job_id,
                        cmd=bbmap_cmds[seqfile.path], nodes=1, ppn=16,
                        mem="99000m", walltime=10800,
                        depend=bbmap_spades_dependencies)
            parser_dependencies.append(bbmap_job_id)

        # Adding spades jobs.
        if(args.spades):
            spades_job_id = seqfile.filename + "_spades"
            assembly_mem = str(args.assembly_mem) + "gb"
            jobs.addJob(jobid=spades_job_id,
                        cmd=spades_cmds[seqfile.path], nodes=1, ppn=4,
                        mem=assembly_mem, walltime=345600,
                        depend=bbmap_spades_dependencies)
            parser_dependencies.append(spades_job_id)

        # Adding output parser job.
        jobs.addJob(jobid=seqfile.filename + "_out_parser",
                    cmd=parser_cmds[seqfile.path], nodes=1, ppn=1,
                    mem="1900m", walltime=600, depend=parser_dependencies)

    if(args.wait):
        jobs.run(job_wait=True, max_wait=args.wait)
    else:
        jobs.run()

# No queue, run commands one at a time.
else:
    for seqfile in files:
        # Run trimming
        subprocess.run(trim_cmds[seqfile.path], shell=True)
        # Running FastQC
        subprocess.run(fastqc_cmds[seqfile.path], shell=True)
        # Running BBMap
        subprocess.run(bbmap_cmds[seqfile.path], shell=True)
        # Running SPAdes
        subprocess.run(spades_cmds[seqfile.path], shell=True)
        # Running output parser
        subprocess.run(parser_cmds[seqfile.path], shell=True)

quit(0)
